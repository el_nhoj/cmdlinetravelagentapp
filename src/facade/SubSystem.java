package facade;

import java.util.Scanner;

import model.soa.Accommodation;
import model.soa.DataBindingUtils;
import model.soa.Flight;
import model.soa.TravelAgent;
import stubs.AccommodationServiceStub;
import stubs.AccommodationServiceStub.BookingConfirmationAccommodation;
import stubs.AirlineServiceStub;
import stubs.AirlineServiceStub.BookingConfirmationAirline;

public class SubSystem {
	private Scanner console = new Scanner(System.in);
	private model.soa.BookingConfirmationAirline airlineBooking;
	private model.soa.BookingConfirmationAccommodation accommBooking;

	public SubSystem() {
	}

	// Calls the Airline Service and its methods to book a flight
	public void bookFlightPrompt() {
		AirlineServiceStub stub;
		try {
			stub = new AirlineServiceStub(
					"http://localhost:8080/axis2/services/AirlineService");
			String user = getUserInput("Please enter username: ");
			String pass = getUserInput("Please enter password: ");
			// Attempt to login as a TravelAgent with provided string inputs
			AirlineServiceStub.TravelAgent stubAgent = stub.login(user, pass);
			if (stubAgent != null) {
				System.out.println("Authentication successful.\n");
				// Convert to model.soa.TravelAgent object
				TravelAgent travelAgent = DataBindingUtils
						.toSOATravelAgent(stubAgent);
				System.out.println("Welcome " + travelAgent.getName()
						+ " here is a list of today's specials\n");
				// Call method to retrieve an array of stub flights and print
				// their information
				AirlineServiceStub.Flight[] stubFlights = stub.getFlights();
				showFlights(stubFlights);
				boolean match = false;
				AirlineServiceStub.Flight stubFlight = null;
				// Enter flight Id and find matching stubFlight object in array
				while (!match) {
					String flightId = getUserInput("Please enter a flight Id: ");
					for (int i = 0; i < stubFlights.length; i++) {
						if (flightId.equalsIgnoreCase(stubFlights[i].getId())) {
							stubFlight = stubFlights[i];
							match = true;
						}
					}
				}
				System.out.println("Flight Selected: " + stubFlight.getId()
						+ " flying to " + stubFlight.getDestination());
				// Create a stub Customer object
				String custName = getUserInput("Please enter customer name: ");
				String custPW = getUserInput("Please enter customer password: ");
				String custAddress = getUserInput("Please enter customer address: ");
				AirlineServiceStub.Customer stubCustomer = stub.createCustomer(
						custName, custPW, custAddress);
				// Attempt to create stub BookingConfirmationAirline object with
				// required stub TravelAgent, Flight and Customer objects
				AirlineServiceStub.BookingConfirmationAirline stubBooking = stub
						.bookFlight(stubAgent, stubFlight, stubCustomer);
				if (stubBooking != null) {
					// Attempt to convert stub objects to bean objects and print
					airlineBooking = DataBindingUtils
							.toSOABookingConfirmation(stubBooking);
					printBooking(airlineBooking);
				} else {
					System.out
							.println("Travel Agent authentication failed. Booking cancelled.");
				}
			} else {
				System.out.println("Travel Agent authentication failed.\n");
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	// Calls the Accommodation Service and its methods to book an accommodation
	public void bookAccommodationPrompt() {
		AccommodationServiceStub stub;
		try {
			stub = new AccommodationServiceStub(
					"http://localhost:8080/axis2/services/AccommodationService");
			String user = getUserInput("Please enter username: ");
			String pass = getUserInput("Please enter password: ");
			// Attempt to login as a TravelAgent with provided string inputs
			AccommodationServiceStub.TravelAgent stubAgent = stub.login(user,
					pass);
			if (stubAgent != null) {
				System.out.println("Authentication successful.\n");
				// Convert to model.soa.TravelAgent object
				TravelAgent travelAgent = DataBindingUtils
						.toSOATravelAgent(stubAgent);
				System.out.println("Welcome " + travelAgent.getName()
						+ " here is a list of accommodation options\n");
				// Display list of destinations and get input
				String destination = showDestination();
				// Call method to retrieve an array of stub Accommodations and
				// print their information
				AccommodationServiceStub.Accommodation[] stubAccommodations = stub
						.getAccommodationList(destination);
				showAccommodations(stubAccommodations);
				boolean match = false;
				AccommodationServiceStub.Accommodation stubAccommodation = null;
				// Search accommodation name and find matching stub
				// Accommodation object in array
				while (!match) {
					String hotel = getUserInput("Please enter an accommodation name: ");
					for (int i = 0; i < stubAccommodations.length; i++) {
						if (stubAccommodations[i].getName().toLowerCase()
								.contains(hotel.toLowerCase())) {
							stubAccommodation = stubAccommodations[i];
							match = true;
						}
					}
				}
				System.out.println("Accommodation Selected: "
						+ stubAccommodation.getName());
				// Create a stub Customer object
				String custName = getUserInput("Please enter customer name: ");
				String custPW = getUserInput("Please enter customer password: ");
				String custAddress = getUserInput("Please enter customer address: ");
				AccommodationServiceStub.Customer stubCustomer = stub
						.createCustomer(custName, custPW, custAddress);
				// Attempt to create stub BookingConfirmationAccommodation
				// object with required stub TravelAgent, Accommodation and
				// Customer objects
				AccommodationServiceStub.BookingConfirmationAccommodation stubBooking = stub
						.bookAccommodation(stubAgent, stubAccommodation,
								stubCustomer);
				if (stubBooking != null) {
					// Attempt to convert stub objects to bean objects and print
					accommBooking = DataBindingUtils
							.toSOABookingConfirmationAccommodation(stubBooking);
					printBooking(accommBooking);
				} else {
					System.out
							.println("Travel Agent authentication failed. Booking cancelled.");
				}
			} else {
				System.out.println("Travel Agent authentication failed.\n");
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	// Print out the server's array of stub flight information
	public void showFlights(AirlineServiceStub.Flight[] stubFlights) {
		for (AirlineServiceStub.Flight f : stubFlights) {
			System.out.println("Airline: " + f.getAirlineName() + "\n"
					+ "Flight Id: " + f.getId() + "\n" + "Destination: "
					+ f.getDestination() + "\n" + "Departure Date: "
					+ f.getDepartureDate() + "\n" + "Departure Gate: "
					+ f.getDepartureGate() + "\n");
		}
	}

	// Print out the server's array of accommodation information
	public void showAccommodations(
			AccommodationServiceStub.Accommodation[] stubAccommodations) {
		for (AccommodationServiceStub.Accommodation a : stubAccommodations) {
			System.out.println("Hotel: " + a.getName() + "\n" + "Address: "
					+ a.getAddress() + "\n" + "Phone Number: "
					+ a.getPhoneNumber() + "\n");
		}
	}

	// Display list of destinations and convert input choice to return a
	// destination string
	public String showDestination() {
		String destination = "";
		boolean running = true;
		do {
			try {
				int option = showMenu("Please select a destination: ",
						new String[] { "Broome", "Dubai", "Singapore",
								"Bangkok" });
				switch (option) {
				case 1:
					destination = "Broome";
					running = false;
					break;
				case 2:
					destination = "Dubai";
					running = false;
					break;
				case 3:
					destination = "Singapore";
					running = false;
					break;
				case 4:
					destination = "Bangkok";
					running = false;
					break;
				default:
					System.out.println("Invalid option.");
					break;
				}
			} catch (Exception e) {
				System.out.println("Error: " + e.getMessage());
			}
		} while (running);
		return destination;
	}

	// Prints BookingConfirmationAirline details
	public void printBooking(model.soa.BookingConfirmationAirline airlineBooking) {
		System.out.println("\nThank you "
				+ airlineBooking.getCustomer().getName() + " for booking with "
				+ airlineBooking.getFlight().getAirlineName() + " flying to "
				+ airlineBooking.getFlight().getDestination() + ".\n"
				+ "Your Flight Details are: \n" + "Your Booking Id is: "
				+ airlineBooking.getId() + "\nFlight Number: "
				+ airlineBooking.getFlight().getId() + "\nDeparting on: "
				+ airlineBooking.getFlight().getDepartureDate()
				+ "\nGate Number: "
				+ airlineBooking.getFlight().getDepartureGate() + "\n");
	}

	// Prints BookingConfirmationAccommodation details
	public void printBooking(
			model.soa.BookingConfirmationAccommodation accommodationBooking) {
		System.out.println("\nThank you "
				+ accommodationBooking.getCustomer().getName()
				+ " for booking with "
				+ accommodationBooking.getAccommodation().getName() + " in "
				+ accommodationBooking.getAccommodation().getLocation() + ".\n"
				+ "Your Accommodation Details are: \n" + "Your Booking Id is: "
				+ accommodationBooking.getId() + "\nHotel: "
				+ accommodationBooking.getAccommodation().getName()
				+ "\nAddress: "
				+ accommodationBooking.getAccommodation().getAddress()
				+ "\nPhone: "
				+ accommodationBooking.getAccommodation().getPhoneNumber()
				+ "\n");
	}

	// Print all existing booking confirmation objects
	public void printAllBookingConfirmation() {
		if (airlineBooking != null) {
			printBooking(airlineBooking);
		} else {
			System.out
					.println("\nAirline Booking Confirmation does not exist.");
		}
		if (accommBooking != null) {
			printBooking(accommBooking);
		} else {
			System.out
					.println("Accommodation Booking Confirmation does not exist.\n");
		}
	}

	// Display a menu based on a given title and a String array
	public int showMenu(String title, String menuItems[]) {
		while (true) {
			int i = 0;
			System.out.println(title + "\n");
			for (String item : menuItems) {
				System.out.printf("%s) %s\n", ++i, item);
			}
			System.out.println();
			while (true) {
				System.out.printf("Please enter an option [%s-%s]: ", 1,
						menuItems.length);
				String option = console.nextLine();
				try {
					int input = Integer.parseInt(option);
					if (input > 0 && input <= menuItems.length) {
						return input;
					}
				} catch (NumberFormatException e) {
					System.out.print("Option entered is not a number. ");
					continue;
				}
			}
		}
	}

	// Display a String prompt and return a user's String input
	public String getUserInput(String prompt) {
		while (true) {
			System.out.print(prompt);
			String input = console.nextLine();
			if (input.trim().isEmpty()) {
				continue;
			} else {
				return input;
			}
		}
	}
}
