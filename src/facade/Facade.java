package facade;

// Facade class
public class Facade {
	private SubSystem sub = new SubSystem();

	public Facade() {
	}

	// Start program and run menu
	public void run() {
		boolean running = true;
		do {
			try {
				int option = sub.showMenu("Travel Booking Menu", new String[] {
						"Book Flights", "Book Accommodation",
						"Display All Booking Confirmations", "Quit" });
				switch (option) {
				case 1:
					sub.bookFlightPrompt();
					break;
				case 2:
					sub.bookAccommodationPrompt();
					break;
				case 3:
					sub.printAllBookingConfirmation();
					break;
				case 4:
					System.out.println("Exiting application.");
					running = false;
					break;
				default:
					System.out.println("Invalid option.");
					break;
				}
			} catch (Exception e) {
				System.out.println("Error: " + e.getMessage());
			}
		} while (running);
	}
}
