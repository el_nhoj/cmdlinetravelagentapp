
/**
 * AccommodationServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package stubs;

    /**
     *  AccommodationServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class AccommodationServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public AccommodationServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public AccommodationServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for login method
            * override this method for handling normal response from login operation
            */
           public void receiveResultlogin(
                    stubs.AccommodationServiceStub.TravelAgent result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from login operation
           */
            public void receiveErrorlogin(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getAccommodationList method
            * override this method for handling normal response from getAccommodationList operation
            */
           public void receiveResultgetAccommodationList(
                    stubs.AccommodationServiceStub.Accommodation[] result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getAccommodationList operation
           */
            public void receiveErrorgetAccommodationList(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for bookAccommodation method
            * override this method for handling normal response from bookAccommodation operation
            */
           public void receiveResultbookAccommodation(
                    stubs.AccommodationServiceStub.BookingConfirmationAccommodation result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from bookAccommodation operation
           */
            public void receiveErrorbookAccommodation(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for createCustomer method
            * override this method for handling normal response from createCustomer operation
            */
           public void receiveResultcreateCustomer(
                    stubs.AccommodationServiceStub.Customer result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from createCustomer operation
           */
            public void receiveErrorcreateCustomer(java.lang.Exception e) {
            }
                


    }
    