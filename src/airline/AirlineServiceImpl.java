package airline;

import interfaces.AirlineService;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import model.beans.BookingConfirmationAirline;
import model.beans.Customer;
import model.beans.Flight;
import model.beans.TravelAgent;

public class AirlineServiceImpl implements AirlineService {
	private Map<String, String> accounts;

	public AirlineServiceImpl() {
		// Initialize HashMap of TravelAgent account key/values
		accounts = new HashMap<>();
		accounts.put("SOA Travel", "123");
		accounts.put("Cheap Travel", "456");
		accounts.put("Travel Service Inc", "789");
	}

	@Override
	public TravelAgent login(String username, String password) {
		String expectedPassword = accounts.get(username);
		// If nothing matches
		if (expectedPassword == null) {
			return null;
			// If username and password match, return TravelAgent object with
			// assigned values
		} else if (expectedPassword.equals(password)) {
			String id;
			if (username.equals("SOA Travel")) {
				id = "00000001";
			} else if (username.equals("Cheap Travel")) {
				id = "00000002";
			} else {
				id = "00000003";
			}
			return new TravelAgent(id, username, password);
		} else {
			// If username good, but bad password
			return null;
		}
	}

	@Override
	public Flight[] getFlights() {
		// Initialize and return flightList array
		Flight[] flightList = new Flight[4];
		flightList[0] = new Flight("QF123", "QANTAS", 1000.0, new Date(), "9",
				"Broome");
		flightList[1] = new Flight("EMM456", "Emirates", 1200.0, new Date(),
				"5", "Dubai");
		flightList[2] = new Flight("SIN764", "Singapore", 5000.0, new Date(),
				"3", "Singapore");
		flightList[3] = new Flight("THA999", "Thai Airways", 500.0, new Date(),
				"1", "Bangkok");
		return flightList;
	}

	@Override
	public BookingConfirmationAirline bookFlight(TravelAgent travelAgent,
			Flight flight, Customer customer) {
		String expectedPassword = accounts.get(travelAgent.getName());
		// If nothing matches
		if (expectedPassword == null) {
			return null;
			// If username and password match, return
			// BookingConfirmationAirline object
		} else if (expectedPassword.equals(travelAgent.getPassword())) {
			BookingConfirmationAirline airBooking = new BookingConfirmationAirline(
					flight, customer);
			airBooking.setId(UUID.randomUUID().toString());
			return airBooking;
		} else {
			// If username good but bad password
			return null;
		}
	}

	// Create a Customer object method
	public Customer createCustomer(String name, String password, String address) {
		String customerId = UUID.randomUUID().toString();
		return new Customer(customerId, name, password, address);
	}
}
