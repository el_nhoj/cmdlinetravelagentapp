package model.soa;

public class BookingConfirmationAirline extends BookingConfirmation {
	private String id;
	protected Customer customer;
	private Flight flight;

	public BookingConfirmationAirline(Flight flight, Customer customer) {
		this.flight = flight;
		this.customer = customer;
	}

	public BookingConfirmationAirline() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Flight getFlight() {
		return flight;
	}

	public void setFlight(Flight flight) {
		this.flight = flight;
	}

	public String toString() {
		return getCustomer().getName() + ":" + getFlight().getAirlineName()
				+ ":" + getFlight().getDestination() + ":"
				+ getFlight().getId() + ":" + getFlight().getDepartureGate();
	}

	public void setFlight(stubs.AirlineServiceStub.Flight flight) {
		this.flight = DataBindingUtils.toSOAFlight(flight);
	}

	public void setCustomer(
			stubs.AirlineServiceStub.BookingConfirmationAirline airlineBookingConfirmation) {
		this.customer = DataBindingUtils
				.toSOACustomer(airlineBookingConfirmation);
	}
}
