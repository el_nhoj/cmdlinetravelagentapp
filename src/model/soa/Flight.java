package model.soa;

import java.util.Date;

public class Flight {
	private String id;
	private String airlineName;
	private Date departureDate;
	private String departureGate;
	private String destination;
	private double cost;

	public Flight(String flightId, String airlineName, double cost,
			Date departureDate, String departureGate, String destination) {
		this.id = flightId;
		this.airlineName = airlineName;
		this.cost = cost;
		this.departureDate = departureDate;
		this.departureGate = departureGate;
		this.destination = destination;
	}

	public Flight() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAirlineName() {
		return airlineName;
	}

	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getDepartureGate() {
		return departureGate;
	}

	public void setDepartureGate(String departureGate) {
		this.departureGate = departureGate;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public String toString() {
		return id + ":" + airlineName + ":" + departureGate + ":" + destination
				+ ":" + cost;
	}
}
