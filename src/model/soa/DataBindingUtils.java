package model.soa;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.beanutils.BeanUtils;

import stubs.AccommodationServiceStub;
// import stubs.ActivitiesServiceStub;
import stubs.AirlineServiceStub;

// Convert between the various paramater types used by adb framework
// makes sense since these would all be from different business and we expect to convert
public class DataBindingUtils {
	// Customer Conversions
	public static model.soa.Customer toSOACustomer(
			AirlineServiceStub.BookingConfirmation bookingConfirmation) {
		model.soa.Customer customer = new model.soa.Customer();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(customer,
					bookingConfirmation.getCustomer());
		} catch (Exception e) {
			logError(e);
		}
		assert customer != null : "data binding error with Customer bean";
		return customer;
	}

	public static model.soa.Customer toSOACustomer(
			AccommodationServiceStub.BookingConfirmation bookingConfirmation) {
		model.soa.Customer customer = new model.soa.Customer();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(customer,
					bookingConfirmation.getCustomer());
		} catch (Exception e) {
			logError(e);
		}
		assert customer != null : "data binding error with Customer bean";
		return customer;
	}

	public static AirlineServiceStub.Customer toAirlineCustomer(
			model.soa.Customer customer) {
		AirlineServiceStub.Customer airlineCustomer = new AirlineServiceStub.Customer();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(airlineCustomer, customer);
		} catch (Exception e) {
			logError(e);
		}
		assert airlineCustomer != null : "data binding error with Customer bean";
		return airlineCustomer;
	}

	public static AccommodationServiceStub.Customer toAccommodationCustomer(
			model.soa.Customer customer) {
		AccommodationServiceStub.Customer accommodationCustomer = new AccommodationServiceStub.Customer();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(accommodationCustomer, customer);
		} catch (Exception e) {
			logError(e);
		}
		assert accommodationCustomer != null : "data binding error with Customer bean";
		return accommodationCustomer;
	}

	// Flight Conversions
	public static model.soa.Flight toSOAFlight(
			AirlineServiceStub.Flight airlineFlight) {
		model.soa.Flight flight = new model.soa.Flight();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(flight, airlineFlight);
		} catch (Exception e) {
			logError(e);
		}
		assert flight != null : "data binding error with Flight bean";
		return flight;
	}

	public static AirlineServiceStub.Flight toAirlineFlight(
			model.soa.Flight flight) {
		AirlineServiceStub.Flight airlineFlight = new AirlineServiceStub.Flight();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(airlineFlight, flight);
		} catch (Exception e) {
			logError(e);
		}
		assert airlineFlight != null : "data binding error with Flight bean";
		return airlineFlight;
	}

	// Accommodation Conversions
	// Accommodation Conversions
	public static model.soa.Accommodation toSOAAccommodation(
			AccommodationServiceStub.Accommodation accommodationAccommodation) {
		model.soa.Accommodation accommodation = new model.soa.Accommodation();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(accommodation, accommodationAccommodation);
		} catch (Exception e) {
			logError(e);
		}
		assert accommodation != null : "data binding error with Accommodation bean";
		return accommodation;
	}

	public static AccommodationServiceStub.Accommodation toAccommodationAccommodation(
			model.soa.Accommodation accommodation) {
		AccommodationServiceStub.Accommodation accommodationAcccommodation = new AccommodationServiceStub.Accommodation();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils
					.copyProperties(accommodationAcccommodation, accommodation);
		} catch (Exception e) {
			logError(e);
		}
		assert accommodationAcccommodation != null : "data binding error with Accommodation bean";
		return accommodationAcccommodation;
	}

	// Booking Confirmation Conversions
	// Booking Confirmation Conversions
	public static model.soa.BookingConfirmationAirline toSOABookingConfirmation(
			AirlineServiceStub.BookingConfirmationAirline airlineBookingConfirmation) {
		model.soa.BookingConfirmationAirline bookingConfirmation = new model.soa.BookingConfirmationAirline();
		try {
			// uses commons-beanutils-xxx.jar in lib/
//			BeanUtils.copyProperties(bookingConfirmation,
//					airlineBookingConfirmation);
			// String a =
			// airlineBookingConfirmation.getFlight().getAirlineName();
			bookingConfirmation.setId(airlineBookingConfirmation.getId());
			bookingConfirmation.setFlight(airlineBookingConfirmation
					.getFlight());
			bookingConfirmation.setCustomer(airlineBookingConfirmation);
		} catch (Exception e) {
			logError(e);
		}
		assert bookingConfirmation != null : "data binding error with Booking confirmation bean";
		return bookingConfirmation;
	}

	public static model.soa.BookingConfirmationAccommodation toSOABookingConfirmationAccommodation(
			AccommodationServiceStub.BookingConfirmationAccommodation accommodationBookingConfirmation) {
		model.soa.BookingConfirmationAccommodation bookingConfirmation = new model.soa.BookingConfirmationAccommodation();
		try {
			// uses commons-beanutils-xxx.jar in lib/
//			 BeanUtils.copyProperties(bookingConfirmation,
//			 accommodationBookingConfirmation);
//			 String a =
//			 accommodationBookingConfirmation.getAccommodation().getName();
			bookingConfirmation.setId(accommodationBookingConfirmation.getId());
			bookingConfirmation
					.setAccommodation(accommodationBookingConfirmation
							.getAccommodation());
			bookingConfirmation.setCustomer(accommodationBookingConfirmation);
		} catch (Exception e) {
			logError(e);
		}
		assert bookingConfirmation != null : "data binding error with Booking confirmation bean";
		return bookingConfirmation;
	}

	// Travel Agent Conversions
	// Travel Agent Conversions
	public static TravelAgent toSOATravelAgent(
			AirlineServiceStub.TravelAgent airlineTravelAgent) {
		model.soa.TravelAgent travelAgent = new model.soa.TravelAgent();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(travelAgent, airlineTravelAgent);
		} catch (Exception e) {
			logError(e);
		}
		assert travelAgent != null : "data binding error with Travel Agent bean";
		return travelAgent;
	}

	public static TravelAgent toSOATravelAgent(
			AccommodationServiceStub.TravelAgent accommodationTravelAgent) {
		model.soa.TravelAgent travelAgent = new model.soa.TravelAgent();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(travelAgent, accommodationTravelAgent);
		} catch (Exception e) {
			logError(e);
		}
		assert travelAgent != null : "data binding error with Travel Agent bean";
		return travelAgent;
	}

	public static AirlineServiceStub.TravelAgent toAirlineTravelAgent(
			model.soa.TravelAgent travelAgent) {
		AirlineServiceStub.TravelAgent airlineTravelAgent = new AirlineServiceStub.TravelAgent();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(airlineTravelAgent, travelAgent);
		} catch (Exception e) {
			logError(e);
		}
		assert airlineTravelAgent != null : "data binding error with Flight bean";
		return airlineTravelAgent;
	}

	public static AccommodationServiceStub.TravelAgent toAccommodationTravelAgent(
			model.soa.TravelAgent travelAgent) {
		AccommodationServiceStub.TravelAgent accommodationTravelAgent = new AccommodationServiceStub.TravelAgent();
		try {
			// uses commons-beanutils-xxx.jar in lib/
			BeanUtils.copyProperties(accommodationTravelAgent, travelAgent);
		} catch (Exception e) {
			logError(e);
		}
		assert accommodationTravelAgent != null : "data binding error with Flight bean";
		return accommodationTravelAgent;
	}

	public static void logError(Exception e) {
		// Use built in Java logger
		Logger logger = Logger.getLogger("assignment1");
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String exceptionAsString = sw.toString();
		logger.log(Level.SEVERE, exceptionAsString);
	}
}
