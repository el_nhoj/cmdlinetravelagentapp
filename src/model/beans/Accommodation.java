package model.beans;

public class Accommodation {
	private String id;
	private String name;
	private String address;
	private String phoneNumber;
	private String location;

	public Accommodation(String id, String name, String address,
			String phoneNumber, String location) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.phoneNumber = phoneNumber;
		this.location = location;
	}

	public Accommodation() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String toString() {
		return id + ":" + name + ":" + address + ":" + phoneNumber + ":"
				+ location;
	}
}
