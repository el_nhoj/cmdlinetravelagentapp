package model.beans;

public class BookingConfirmation {
	private String id;
	protected Customer customer;

	public BookingConfirmation(Customer customer) {
		this.customer = customer;
	}

	public BookingConfirmation() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	} 
}
