package interfaces;

import model.beans.BookingConfirmationAirline;
import model.beans.Customer;
import model.beans.Flight;
import model.beans.TravelAgent;

public interface AirlineService {
	/**
	 * @param username
	 *            the travel agent accessing the service
	 * @param password
	 *            the travel agent accessing the service
	 * @return a Travel Agent token that can be used for authenticating to the
	 *         service
	 */
	public abstract TravelAgent login(String username, String password);

	/**
	 * @return an collection of Flights to display to the user for purchase
	 */
	public abstract Flight[] getFlights();

	/**
	 * @param travelAgent
	 *            the travel agent accessing the service
	 * @param flight
	 *            the flight their customer wants to book
	 * @param customer
	 *            the customer that will be traveling
	 * @return a booking confirmation slip with details of the flight
	 */
	public abstract BookingConfirmationAirline bookFlight(
			TravelAgent travelAgent, Flight flight, Customer customer);
}