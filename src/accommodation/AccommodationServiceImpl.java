package accommodation;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import model.beans.Accommodation;
import model.beans.BookingConfirmationAccommodation;
import model.beans.Customer;
import model.beans.TravelAgent;
import interfaces.AccommodationService;

public class AccommodationServiceImpl implements AccommodationService {
	private Map<String, String> accounts;

	public AccommodationServiceImpl() {
		// Initialize HashMap of TravelAgent account key/values
		accounts = new HashMap<>();
		accounts.put("SOA Travel", "123");
		accounts.put("Cheap Travel", "456");
		accounts.put("Travel Service Inc", "789");
	}

	@Override
	public TravelAgent login(String username, String password) {
		String expectedPassword = accounts.get(username);
		// If nothing matches
		if (expectedPassword == null) {
			return null;
			// If username and password match, return TravelAgent object with
			// assigned values
		} else if (expectedPassword.equals(password)) {
			String id;
			if (username.equals("SOA Travel")) {
				id = "00000001";
			} else if (username.equals("Cheap Travel")) {
				id = "00000002";
			} else {
				id = "00000003";
			}
			return new TravelAgent(id, username, password);
		} else {
			// If username good, but bad password
			return null;
		}
	}

	@Override
	public model.beans.Accommodation[] getAccommodationList(String destination) {
		// Initialize and return accommodationList array, populate information
		// based on destination
		Accommodation[] accommodationList = new Accommodation[4];
		for (int i = 0; i < accommodationList.length; i++) {
			switch (destination) {
			case "Broome":
				accommodationList[0] = new Accommodation(
						"64dbb281-3677-4089-a7e0-9c64a2fe3f14",
						"Cable Beach Resort",
						"1 Cable Beach Road | Cable Beach, Broome, Western Australia 6726",
						"(08) 9192 0400", "Broome");
				accommodationList[1] = new Accommodation(
						"a56039ad-8f52-4044-af31-9d2f4e596a4f",
						"Mantra Frangipani",
						"15 Millington Drive | Cable Beach, Broome, Western Australia 6726",
						"(08) 9192 0401", "Broome");
				accommodationList[2] = new Accommodation(
						"81ce6f6c-9118-41ed-a72b-2984eee145de",
						"Bali Hai Resort & Spa",
						"6 Murray Rd, Cable Beach, Broome, Western Australia 6726",
						"(08) 9192 0402", "Broome");
				accommodationList[3] = new Accommodation(
						"2db135ef-7848-439a-9672-945c2653f5bd", "Oaks",
						"99 Robinson Street, Broome, Western Australia 6725",
						"(08) 9192 0403", "Broome");
				break;
			case "Dubai":
				accommodationList[0] = new Accommodation(
						"72a29c92-40b2-4637-a008-3787ee27a33a",
						"Atlantis, The Palm",
						"Crescent Road | The Palm, Dubai, United Arab Emirates",
						"0011 971 4 426 0000", "Dubai");
				accommodationList[1] = new Accommodation(
						"6fec1c6d-d6a0-4d43-ad18-02c09fd38c71",
						"Radisson Blu Hotel, Dubai Deira Creek",
						"Bani Yas Road, Dubai 476, United Arab Emirates",
						"0011 971 4 222 7171", "Dubai");
				accommodationList[2] = new Accommodation(
						"92a17a3c-5487-4519-b3ed-af1af86afc68",
						"Shangri-La Hotel, Dubai",
						"Sheikh Zayed Road | PO Box 75880, Dubai, United Arab Emirates",
						"0011 971 4 222 7172", "Dubai");
				accommodationList[3] = new Accommodation(
						"53a6e314-2db9-439b-96bb-ce512db2de4d",
						"Arabian Courtyard Hotel & Spa",
						"Al Fahidi Street, Dubai, United Arab Emirates",
						"0011 971 4 222 7172", "Dubai");
				break;
			case "Singapore":
				accommodationList[0] = new Accommodation(
						"74f3a4e8-2c4c-4ea4-8c42-7ea39b31b78f",
						"Shangri-La Hotel, Singapore",
						"22 Orange Grove Rd, Singapore 258350",
						"0011 65 6505 5663", "Singapore");
				accommodationList[1] = new Accommodation(
						"d1f24ead-b1b8-4a91-9a56-806dbd52a55b",
						"InterContinental",
						"80 Middle Rd, Singapore 188966, Singapore",
						"0011 65 6505 5664", "Singapore");
				accommodationList[2] = new Accommodation(
						"10ab2142-3925-4492-83ff-403a4f2fe927",
						"Carlton Hotel",
						"76 Bras Basah Rd, Singapore 189558, Singapore",
						"0011 65 6505 5665", "Singapore");
				accommodationList[3] = new Accommodation(
						"1e4c9c0f-e2fb-4a31-b11f-7f1ad28a99c5", "Park Royal",
						"7500 Beach Road, Singapore 199591, Singapore",
						"0011 65 6505 5666", "Singapore");
				break;
			case "Bangkok":
				accommodationList[0] = new Accommodation(
						"8e1e45d5-6f58-43cf-b912-0a696b4a627c",
						"Shangri-La Hotel",
						"89 Soi Wat Suan Plu, New Road | Bangrak, Bangkok 10500, Thailand",
						"1-800-760-594", "Bangkok");
				accommodationList[1] = new Accommodation(
						"335dfb3b-6723-4f31-a33b-eba8f78273b3",
						"Holiday Inn",
						"1 Sukhumvit 22 | Klongton, Klongtoey, Bangkok 10110, Thailand",
						"1-800-760-595", "Bangkok");
				accommodationList[2] = new Accommodation(
						"49b5208c-b115-426c-8f21-383711f6f5a1",
						"Oriental Residence",
						"110 Wireless Road | Lumpini, Bangkok 10330, Thailand",
						"1-800-760-593", "Bangkok");
				accommodationList[3] = new Accommodation(
						"f5a77cca-82d6-4b34-95c4-642230dd1129",
						"Amari Watergate",
						"847 Phetchaburi Road, Bangkok 10400, Thailand",
						"1-800-760-592", "Bangkok");
				break;
			default:
				break;
			}
		}
		return accommodationList;
	}

	@Override
	public BookingConfirmationAccommodation bookAccommodation(
			TravelAgent travelAgent, Accommodation accommodation,
			Customer customer) {
		String expectedPassword = accounts.get(travelAgent.getName());
		// If nothing matches
		if (expectedPassword == null) {
			return null;
			// If username and password match, return
			// BookingConfirmationAccommodation object
		} else if (expectedPassword.equals(travelAgent.getPassword())) {
			BookingConfirmationAccommodation accommBooking = new BookingConfirmationAccommodation(
					accommodation, customer);
			accommBooking.setId(UUID.randomUUID().toString());
			return accommBooking;
		} else {
			// If username good, but bad password
			return null;
		}
	}

	// Create a Customer object method
	public Customer createCustomer(String name, String password, String address) {
		String customerId = UUID.randomUUID().toString();
		return new Customer(customerId, name, password, address);
	}
}
