package client;

import facade.Facade;

import model.soa.Accommodation;
import model.soa.DataBindingUtils;
import model.soa.Flight;
import model.soa.TravelAgent;
import stubs.AccommodationServiceStub;
import stubs.AccommodationServiceStub.BookingConfirmationAccommodation;
import stubs.AirlineServiceStub;
import stubs.AirlineServiceStub.BookingConfirmationAirline;

// Assignment skeleton by Rodney Cocker for Web Services
// based on work by Dr Caspar Ryan
// semester 1, 2015
public class Client {
	public static void main(String args[]) {
		Facade facade = new Facade();
		facade.run();
	}
}
